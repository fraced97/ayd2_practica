import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearFileComponent } from './crear-file.component';

describe('CrearFileComponent', () => {
  let component: CrearFileComponent;
  let fixture: ComponentFixture<CrearFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
