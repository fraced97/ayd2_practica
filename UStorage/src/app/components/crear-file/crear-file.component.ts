import { Component, OnInit } from '@angular/core';
import { Archivo } from 'src/app/models/file';
import { Respuesta } from 'src/app/models/user';
import { FilesService } from 'src/app/services/files.service';

interface HtmlInputEvent extends Event{
  target: HTMLInputElement & EventTarget
}

@Component({
  selector: 'app-crear-file',
  templateUrl: './crear-file.component.html',
  styleUrls: ['./crear-file.component.css']
})

export class CrearFileComponent implements OnInit {
  fotoSelect:File
  archivo:Archivo = {
    bases64 : '',
    extension : '',
    nombre : '',
    password : '',
    visibilidad : '0'
  }
  nombre:string
  oculto:boolean = true
  clase:string = 'danger'
  msj:string
  password:string
  visibilidad:string = '0'

  constructor(private servicio:FilesService) { }

  ngOnInit(): void {
  }

  seleccionarArchivo(event: HtmlInputEvent):void{
    if (event.target.files && event.target.files[0]){
      this.fotoSelect = <File>event.target.files[0]
      console.log(this.fotoSelect.name)
      //para previsualizar
      const reader = new FileReader()
      reader.onload = e => this.archivo.bases64 = reader.result
      reader.readAsDataURL(this.fotoSelect)
    }
  }

  limpiar(){
    this.nombre = ""
    this.password = ""
    this.visibilidad = '0'
    this.archivo = {}
  }

  subir(){
    
    this.archivo.extension = this.fotoSelect.name.split('.')[1]
    console.log(this.visibilidad)
    this.clase = 'danger'
    if (!this.nombre) this.notificar('Debe darle un nombre al archivo')
    else if(!this.password) this.notificar('Debe confirmar la subida del archivo con su contraseña')
    else {
      this.archivo.nombre = this.nombre
      this.archivo.password = this.password
      this.archivo.visibilidad = this.visibilidad
      let b64  = String(this.archivo.bases64).split(';')
      console.log(b64[1])
      this.archivo.bases64 = ''
      for (let index = 1; index < b64.length; index++) if(b64[index]) this.archivo.bases64 += b64[index]
      this.archivo.bases64 = String(this.archivo.bases64).substring(7)
      console.log(this.archivo)
      this.servicio.crear(this.archivo).subscribe(
        (res:Respuesta)=>{
          if(res.status === 200) {
            this.clase = 'success'
            this.limpiar()
          }
          this.notificar(res.msj)
        }
      )
    }
    console.log(this.msj)
  }

  notificar(mensaje:string){
    this.msj = mensaje
    this.oculto = false
  }
}
