import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesFriendsComponent } from './files-friends.component';

describe('FilesFriendsComponent', () => {
  let component: FilesFriendsComponent;
  let fixture: ComponentFixture<FilesFriendsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilesFriendsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
