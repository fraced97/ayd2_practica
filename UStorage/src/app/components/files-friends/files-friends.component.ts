import { Component, OnInit } from '@angular/core';
import { Respuesta, RespuestaLogin } from 'src/app/models/user';
import { FriendService } from 'src/app/services/friend.service';

@Component({
  selector: 'app-files-friends',
  templateUrl: './files-friends.component.html',
  styleUrls: ['./files-friends.component.css']
})
export class FilesFriendsComponent implements OnInit {

  amigo:number
  friends:RespuestaLogin[] = []

  constructor(private servicio:FriendService) { }

  ngOnInit(): void {
    this.servicio.obtenerAmigos().subscribe(
      (res:Respuesta) =>{
        console.log(res)
        this.friends = res.data as RespuestaLogin[]
        console.log(this.friends)
        this.amigo = this.friends[0].id_user
      } 
    )
  }


}
