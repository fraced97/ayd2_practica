import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { emit } from 'process';
import { Archivo, editFile } from 'src/app/models/file';
import { Respuesta } from 'src/app/models/user';
import { FilesService } from 'src/app/services/files.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {
  @Input() amigo: number
  @Output() fileEvent = new EventEmitter<editFile>();

  files:Archivo[] 
  vista:Archivo[] = []
  busqueda:string
  visible:string = "Archivos publicos"
  titulo:string
  visibilidad:boolean = false

  constructor(
    private servicio:FilesService,
    private fin:UserService
  ) { }

  ngOnInit(): void {
    this.show()
  }

  logOut(){
    this.fin.logout()
  }

  buscar(){
    if(this.amigo != 0 && this.amigo){
      this.servicio.buscar(this.amigo,this.busqueda,'0').subscribe(
        (res:Respuesta)=>{
          this.files = res.data as Archivo[]
          console.log(res)
        } 
      )
    }else{
      if (this.visibilidad) {        
        this.servicio.buscar(0,this.busqueda,'0').subscribe(
          (res:Respuesta)=> {
            console.log(res)
            this.files = res.data as Archivo[]
          }
        )
      }else{
        this.servicio.buscar(0,this.busqueda,'1').subscribe(
          (res:Respuesta)=> {
            console.log('privados')
            console.log(res)
            this.files = res.data as Archivo[]
          }
        )}
    }
  }

  show(){
    console.log(this.amigo)
    if(this.amigo != 0 && this.amigo){
      this.servicio.obtenerArchivos(this.amigo,'0').subscribe(
        (res:Respuesta)=>{
          console.log(res)
          this.files = res.data as Archivo[]
        } 
      )
    }else{
      if (this.visibilidad) {  
        this.visible = 'Archivos publicos'
        this.titulo = 'Archivos privados'       
        console.log('en privados')     
        this.servicio.obtenerArchivos(0,'1').subscribe(
          (res:Respuesta)=> {this.files = res.data as Archivo[]
            console.log(res.status)
          }
        )
      }else{     
        console.log('en publicos')  
        this.titulo = 'Archivos publicos'
        this.visible = 'Archivos privados'
        this.servicio.obtenerArchivos(0,'0').subscribe(
          (res:Respuesta)=> this.files = res.data as Archivo[]
        )
      }
      this.visibilidad = !this.visibilidad
    }
  }

  verTodos(){
    if (this.amigo == 0 || !this.amigo) {
      this.visibilidad = !this.visibilidad      
    }
    this.show()
  }

  seleccionar(name:string, arch:string, id:number, ext:string, vision:string){
    console.log(this.amigo)
    if(this.amigo && this.amigo!=0) {
      let ruta = 'https://archivos-4-p1.s3.us-east-2.amazonaws.com/'
      if (ext === 'pdf' || ext === 'txt') ruta += 'archivos/'
      else ruta += 'fotos/'
      window.open(ruta + arch + '.' + ext, "_blank");
      return
    }
    this.fileEvent.emit({
      id_file : id,
      amigo: this.amigo,
      original : name,
      nombre : arch,
      visibilidad : vision
    })
  }
}
