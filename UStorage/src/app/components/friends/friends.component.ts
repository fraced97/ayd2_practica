import { Component, OnInit } from '@angular/core';
import { friend, Respuesta } from 'src/app/models/user';
import { FriendService } from 'src/app/services/friend.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
  users:friend[]
  busqueda:string

  constructor(private servicio:FriendService) { }

  ngOnInit(): void {
    this.obtenerUsuarios()
  }

  buscar(){
    if (this.busqueda) {
      this.servicio.buscarUsuario(this.busqueda).subscribe(
        (res:Respuesta) => this.users = res.data as friend[]
      )
    }
  }

  agregar(id_friend:number){
    console.log(id_friend)
    this.servicio.addFriend(id_friend).subscribe(
      (res : Respuesta) => {
        console.log(res.status)
        if(res.status == 200) this.obtenerUsuarios()
      }
    )
  }

  obtenerUsuarios(){
    this.servicio.obtenerUsuarios().subscribe(
      (res:Respuesta) => this.users = res.data as friend[]
    )
  }

}
