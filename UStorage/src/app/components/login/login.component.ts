import { Component, OnInit } from '@angular/core';
import { Login, Respuesta, RespuestaLogin } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  datos:Login = {}
  msj:string
  oculto:boolean = true

  constructor(private servicio:UserService) { }

  ngOnInit(): void {
  }

  loguear():void{
    this.oculto = true
    if (!this.datos.user || !this.datos.password) {
      this.msj = 'Credenciales incompletas'
      this.oculto = false
    } else {      
      this.servicio.loguear(this.datos).subscribe(
        (res:Respuesta)=>{
          if (res.status == 200) {
            let logueado = res.data as RespuestaLogin
            this.servicio.setUsuario(logueado.id_user, logueado.username)
            //this.servicio.setUsuario(logueado.id_user, logueado.username, logueado.foto)
            this.datos = {}
          } else {
            this.msj = res.msj
            this.oculto = false
          }
        }
      )
    }
  }

  ocultar(){this.oculto = true}

}
