import { Component, OnInit } from '@angular/core';
import { editFile } from 'src/app/models/file';
import { Respuesta } from 'src/app/models/user';
import { FilesService } from 'src/app/services/files.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  usuario:string 
  foto:string 
  file:editFile = {}
  editado:editFile = {}
  oculto:boolean = true
  clase:string = 'danger'
  msj:string = ''
  elimina:boolean = false
  archivo:string

  constructor(private servicio:FilesService, private servUser:UserService) { }

  ngOnInit(): void {
    this.foto = this.servUser.getUsuario('foto')
    this.usuario = this.servUser.getUsuario('nickname')
  }

  receiveArchivo($event) {
    this.file = $event
  }

  editar(){
    this.clase = 'danger'
    this.editado.visibilidad = this.file.visibilidad
    this.editado.id_file = this.file.id_file
    console.log(this.editado.visibilidad)
    if (!this.editado.nombre) this.notificar('No le ha dado un nuevo nombre al archivo')
    else if (!this.editado.password) this.notificar('Debe escribir su contraseña para confirmar los cambios')
    else{
      this.servicio.editar(this.editado).subscribe(
        (res:Respuesta) => {
          if (res.status == 200) window.location.reload() 
          else this.notificar(res.msj)
        }
      )
    }
  }

  notificar(m:string){
    this.msj = m
    this.oculto = false
  }

  eliminar(){
    if (!this.file.id_file) return
    this.clase = 'warning'
    this.editado.id_file = this.file.id_file
    if (!this.editado.password) {
      this.notificar('Debe escribir su contraseña para poder eliminar el archivo')
      this.elimina = false
    }
    else{
      this.elimina = !this.elimina
      if (this.elimina) {
        this.archivo = this.file.nombre
        this.notificar('Intenta eliminar un archivo, presione de nuevo el boton eliminar para efectuar la accion')
        return
      }
      if (this.archivo === this.file.nombre) { 
        this.servicio.eliminar(this.editado).subscribe(
          (res:Respuesta)=>{
            if(res.status === 200) window.location.reload()
            else{
              this.notificar(res.msj)
              this.elimina = false
            } 
            
          }
        )       
      }else this.elimina = false
    }
  }

  ver(){
    console.log(this.file.original)
    let ruta = 'https://archivos-4-p1.s3.us-east-2.amazonaws.com/'
    if(this.file.original.endsWith('pdf') || this.file.original.endsWith('txt')) ruta += 'archivos/'
    else ruta += 'fotos/'
    window.open(ruta + this.file.original, "_blank");
  }
}
