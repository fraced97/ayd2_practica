import { Component, OnInit } from '@angular/core';
import { Respuesta, SignUp } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

interface HtmlInputEvent extends Event{
  target: HTMLInputElement & EventTarget
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  datos:SignUp = {}
  valida:string
  msj:string
  oculto:boolean = true
  clase:string
  //fotoSelect:File
  //foto:string | ArrayBuffer

  constructor(private servicio:UserService) { }

  ngOnInit(): void {
  }


  seleccionarFoto(event: HtmlInputEvent):void{
    if (event.target.files && event.target.files[0]){
      //this.fotoSelect = <File>event.target.files[0]
      //this.datos.extension = this.fotoSelect.name.split('.')[1]
      //para previsualizar
      const reader = new FileReader()
      //reader.onload = e => this.foto = reader.result
      //reader.readAsDataURL(this.fotoSelect)
    }
  }

  registrar(){
    this.msj = ''
    this.clase = 'danger'
    this.oculto = true
    console.log(this.datos)
   // if(this.datos.extension) this.datos.foto = this.datos.user
    //if (!this.datos.extension)this.notificar('La foto de perfil es obligatoria')
    if (!this.datos.correo || !this.datos.user || !this.datos.password || !this.datos.nombre) this.notificar('Uno o varios campos no han sido llenados')
    else if (this.valida != this.datos.password) this.notificar('La contraseña no coincide con la confirmacion')
    else{
      //let b64  = String(this.foto).split(';')
      //for (let index = 1; index < b64.length; index++) if(b64[index]) this.datos.bases64 += b64[index]
     // this.datos.bases64 = String(this.datos.bases64).substring(16)
      this.servicio.signup(this.datos).subscribe(
        (res:Respuesta) => {
          if(res.status === 200) this.clase = 'success'
          this.notificar(res.msj)
          this.datos = {}
          this.valida = ""
        }
      )
    } 
  }

  notificar(texto:string){
    this.msj = texto
    this.oculto = false
  }

  ocultar(){
    this.oculto = true
  }

}
