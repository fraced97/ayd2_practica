import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  constructor(private serv:UserService, private router:Router){}
  canActivate(){
    if (this.serv.getUsuario("logueado")){
      return true
    }else{
      this.router.navigate(['/login'])
      return false
    } 
  }
}
