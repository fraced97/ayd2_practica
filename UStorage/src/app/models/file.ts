export interface Respuesta{
    status:number,
    msj:string,
    data?:any
}

export interface Archivo{
    nombre?:string,
    original?:string,
    id_file?:number,
    extension?:string,
    visibilidad?:string,
    id_user?:number,
    bases64?:string | ArrayBuffer,
    password?:string
}

export interface editFile{
    nombre?:string,
    id_user?:number,
    original?:string,
    password?:string,
    id_file?:number,
    visibilidad?:string
    amigo?:number
}