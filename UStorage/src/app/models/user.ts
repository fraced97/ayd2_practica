export interface Login{
    user?:string
    password?:string
}

export interface RespuestaLogin{
    id_user:number,
    username?:string,
    nombre?: string,
    foto?:string
}
export interface SignUp{
    user?:string
	correo?:string
	nombre?:string
	password?:string
	//foto?:string,
    //bases64?:string|ArrayBuffer
    //extension?:string
}

export interface Respuesta{
    status:number,
    msj:string,
    data?:any
}

export interface friend{
    id_user:number,
    username:string,
    extension?:string,
    foto:string,
    cantidad_archivos:number
}
