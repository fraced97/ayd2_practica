import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Archivo, editFile } from '../models/file';

@Injectable({
  providedIn: 'root'
})
export class FilesService {
  api:string = 'http://18.218.96.7:9000/file'
  
  constructor(private http:HttpClient) { }

  obtenerArchivos(user:number,visibilidad:string){
    if(user!= 0) return this.http.get(`${this.api}/?idUsuario=${user}&visibilidad=${visibilidad}`)
    else return this.http.get(`${this.api}?idUsuario=${localStorage.getItem("logueado")}&visibilidad=${visibilidad}`)
  }

  crear(archivo:Archivo){
    archivo.id_user = Number(localStorage.getItem("logueado"))
    return this.http.post(`${this.api}/create`,archivo)
  }

  editar(edit:editFile){
    console.log(edit)
    return this.http.post(`${this.api}/upgrade`,edit)
  }

  eliminar(del:editFile){
    return this.http.post(`${this.api}/delete`,del)
  }

  buscar(user:number, nombre:string, visibilidad:string){
    if(user!= 0) return this.http.get(`${this.api}/search/?idUsuario=${user}&nombre=${nombre}&visibilidad=0`)
    else return this.http.get(`${this.api}/search/?idUsuario=${localStorage.getItem("logueado")}&nombre=${nombre}&visibilidad=${visibilidad}`)
  }

  getUsuario(propiedad:string){
    return localStorage.getItem(propiedad)
  }
}
