import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FriendService {

  api:string = 'http://18.218.96.7:9000/user/'
  constructor(private http:HttpClient) { }
  
  buscarUsuario(username:string){
    return this.http.get(`${this.api}search/${username}`)
  }

  obtenerUsuarios(){
    return this.http.get(`${this.api}${Number(localStorage.getItem("logueado"))}`)
  }

  obtenerAmigos(){
    console.log(`${this.api}friends/${Number(localStorage.getItem("logueado"))}`)
    return this.http.get(`${this.api}friends/${Number(localStorage.getItem("logueado"))}`)
  }


  addFriend(id:number){
    let add = {
      id_friend:id,
      id_user : Number(localStorage.getItem("logueado"))
    }
    console.log(add)
    return this.http.post(`${this.api}agregar`,add)
  }
}
