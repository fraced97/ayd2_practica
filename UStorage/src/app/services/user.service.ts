import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Login, SignUp } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  api_url = "http://3.137.172.112:9000/user/"

  constructor(
    private http:HttpClient,
    private router:Router
  ) { }

  loguear(user:Login){
    return this.http.post(`${this.api_url}login`, user)
  }

  setUsuario(user:number, nickname:string){
    localStorage.setItem("logueado",String(user))
    localStorage.setItem("nickname",nickname)
    //localStorage.setItem("foto",foto)
    this.router.navigate(['/principal'])
  }
  
  getUsuario(propiedad:string){
    return localStorage.getItem(propiedad)
  }
  
  logout(){
    localStorage.removeItem("logueado")
    localStorage.removeItem("nickname")
    //localStorage.removeItem("foto")
    this.router.navigate(['/login'])
  }

  signup(user:SignUp){
    return this.http.post(`${this.api_url}signup`, user)
  }
}
