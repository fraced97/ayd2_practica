var db = require('../models/conexion')
const bcryptjs = require('bcryptjs');
const { v4: uuidv4 } = require('uuid');
const AWS = require('aws-sdk');
const aws_cred = require('../models/aws-keys');
const bucket_s3 = new AWS.S3(aws_cred.s3);

exports.crear = async (req, res) => {
    let body = req.body;
    let id_user = body.id_user;
    let nombreF = body.nombre;
    let extension = body.extension;
    let base64String = body.bases64;
    let visibilidad = body.tipo_archivo;
    let contrasena = body.password;
    //console.log(body)
    
    const sql = `SELECT COUNT(nombre) AS dupli FROM archivo WHERE nombre="${nombreF}" and extension="${extension}";`
    await db.connection.query(sql, function (error, results) {
        if (error) res.send({message: error});
        if(results[0].dupli==0){
            const sql2 = `select * from usuario where id_user = ${id_user};`
            db.connection.query(sql2, function (error, results2) {
                if (error) res.send({message: error});
                let compare = bcryptjs.compareSync(contrasena, results2[0].contrasena)
                if(compare){
                    if(extension=="txt"||extension=="pdf"||extension=="png"||extension=="jpg"||extension=="jpeg"){
                        let foto = uploadfile(nombreF,extension,base64String);
                        let foto2 = `${nombreF}.${extension}`; 
                        const sql3 = `insert into archivo (visibilidad,nombre, extension,id_user,original) values ('${visibilidad}','${nombreF}','${extension}','${id_user}','${foto2}');`
                        db.connection.query(sql3, function (error, results3) {
                        if (error) res.send({message: error});
                        console.log(results3);
                        res.send({
                            'status':200,
                            'msj': 'Archivo creado con exito',
                            'data': []
                            });
                        })
                    }else{
                        console.log("Extension invalida")
                        res.send({
                            'status':500,
                            'msj': 'Extension de del archivo invalido',
                            'data': []
                        });
                    }
                    
                }else{
                    console.log("Contraseña incorrecta")
                    res.send({
                        'status':500,
                        'msj': 'Contraseña incorrecta',
                        'data': []
                    });
                }  
            })
        }else{
            console.log("Archivo ya existe")
            res.send({
                'status':500,
                'msj': 'Archivo ya existe',
                'data': []
            });
        }

    })
}

exports.crear2 = async (req, res) => {
    let body = req.body;
    let id_user = body.id_user;
    let nombreF = body.nombre;
    let extension = body.extension;
    let base64String = body.bases64;
    let visibilidad = body.tipo_archivo;
    let contrasena = body.password;
    //console.log(body)
    
    const sql = `SELECT COUNT(nombre) AS dupli FROM archivo WHERE nombre="${nombreF}" and extension="${extension}";`
    const conn = db.connection.then(conn => {
        conn.query(sql, function (err, results) {
            if (err) throw err;
            if(results[0].dupli==0){
                const sql2 = `select * from usuario where id_user = ${id_user};`
                const conn2 = db.connection.then(conn2 => {
                    conn2.query(sql2, function (err, results2) {
                        if (err) throw err;
                        let compare = bcryptjs.compareSync(contrasena, results2[0].contrasena)
                        if(compare){
                            if(extension=="txt"||extension=="pdf"||extension=="png"||extension=="jpg"||extension=="jpeg"){
                                let foto = uploadfile(nombreF,extension,base64String);
                                let foto2 = `${nombreF}.${extension}`; 
                                const sql3 = `insert into archivo (visibilidad,nombre, extension,id_user,original) values ('${visibilidad}','${nombreF}','${extension}','${id_user}','${foto2}');`
                                const conn3 = db.connection.then(conn3 => {
                                    conn3.query(sql3, function (err, results3) {
                                        if (err) throw err;
                                        console.log(results3);
                                        res.send({
                                            'status':200,
                                            'msj': 'Archivo creado con exito',
                                            'data': []
                                        });
                                    });
                                })
                            }else{
                                console.log("Extension invalida")
                                res.send({
                                    'status':500,
                                    'msj': 'Extension de del archivo invalido',
                                    'data': []
                                });
                            }
                            
                        }else{
                            console.log("Contraseña incorrecta")
                            res.send({
                                'status':500,
                                'msj': 'Contraseña incorrecta',
                                'data': []
                            });
                        }
                    });
                })
            }else{
                console.log("Archivo ya existe")
                res.send({
                    'status':500,
                    'msj': 'Archivo ya existe',
                    'data': []
                });
            }
          });
    })
}

exports.verArchivo = async (req, res) => {
    const sql = `SELECT * FROM archivo where id_user = ? and visibilidad = ?`
    db.connection.query(sql,[req.quey.id_user, req.query.visibilidad], (error, results) => {
        if (error) res.send({message: error});
        if(Object.keys(results).length!=0){
            let user ={
                'status':200,
                'msj': 'Archivos registrados',
                'data': results
            }
            res.send(user);
        }else{
            let user ={
                'status':500,
                'msj': 'No hay archivos',
                'data': results
            }
            res.send(user);
        }
        
    })
}

exports.verArchivo2 = async (req, res) => {
    const sql = `SELECT id_file, nombre, extension, visibilidad FROM archivo where id_user = ? and visibilidad = ?;`
    const conn = db.connection.then(conn => {
        //hacemos la query normal.
        conn.query(sql, [req.query.idUsuario, req.query.visibilidad],function (err, results) {
            if (err) throw err;
            if(Object.keys(results).length!=0){
                let user ={
                    'status':200,
                    'msj': 'Archivos registrados',
                    'data': results
                }
                res.send(user);
            }else{
                let user ={
                    'status':500,
                    'msj': 'No hay archivos',
                    'data': results
                }
                res.send(user);
            }
          });
    })
}

exports.buscarArchivo = async (req, res) => {
    const sql = `SELECT id_file, nombre, extension FROM archivo where id_user = ? and visibilidad = ? and nombre = ?`
    const conn = db.connection.then(conn => {
        //hacemos la query normal.
        conn.query(sql, [req.query.idUsuario, req.query.visibilidad, req.query.nombre],function (err, results) {
            if (err) throw err;
            if(Object.keys(results).length!=0){
                let user ={
                    'status':200,
                    'msj': 'Archivos registrados',
                    'data': results
                }
                res.send(user);
            }else{
                let user ={
                    'status':500,
                    'msj': 'No hay archivos',
                    'data': results
                }
                res.send(user);
            }
          });
    })
}

exports.editarArchivo = async (req, res) => {
    let body = req.body;
    let id_file = body.id_file;
    let nombreF = body.nombre;
    let visibilidad = body.tipo_archivo;
    let contrasena = body.password;
    //console.log(body)
    
    const sql = `SELECT * FROM archivo where id_file = ${id_file};`
    await db.connection.query(sql, function (error, results) {
        if (error) res.send({message: error});
        let id_user = results[0].id_user
        const sql2 = `select * from usuario where id_user = ${id_user};`
        db.connection.query(sql2, function (error, results2) {
            if (error) res.send({message: error});
            let compare = bcryptjs.compareSync(contrasena, results2[0].contrasena)
            if(compare){
                //let foto = uploadfile(nombreF,extension,base64String);
                //let foto2 = `${nombreF}.${extension}`; 
                const sql3 = `UPDATE archivo SET nombre='${nombreF}', visibilidad = '${visibilidad}' WHERE id_file = ${id_file};`
                db.connection.query(sql3, function (error, results3) {
                if (error) res.send({message: error});
                console.log(results3);
                res.send({
                    'status':200,
                    'msj': 'Archivo modifica con exito',
                    'data': []
                    });
                })
                
            }else{
                console.log("Contraseña incorrecta")
                res.send({
                    'status':500,
                    'msj': 'Contraseña incorrecta',
                    'data': []
                });
            }  
        })
        

    })
}

exports.editarArchivo2 = async (req, res) => {
    let body = req.body;
    let id_file = body.id_file;
    let nombreF = body.nombre;
    let visibilidad = body.tipo_archivo;
    let contrasena = body.password;
    //console.log(body)
    
    const sql = `SELECT * FROM archivo where id_file = ${id_file};`
    const conn = db.connection.then(conn => {
        conn.query(sql, function (err, results) {
            if (err) throw err;
            let id_user = results[0].id_user
            const sql2 = `select * from usuario where id_user = ${id_user};`
            const conn2 = db.connection.then(conn2 => {
                conn2.query(sql2, function (err, results2) {
                    if (err) throw err;
                    let compare = bcryptjs.compareSync(contrasena, results2[0].contrasena)
                    if(compare){
                        const sql3 = `UPDATE archivo SET nombre='${nombreF}', visibilidad = '${visibilidad}' WHERE id_file = ${id_file};`
                        const conn3 = db.connection.then(conn3 => {
                            conn3.query(sql3, function (err, results3) {
                                if (err) throw err;
                                console.log(results3);
                                res.send({
                                    'status':200,
                                    'msj': 'Archivo modifica con exito',
                                    'data': []
                                });
                            });
                        })
                        
                        
                    }else{
                        console.log("Contraseña incorrecta")
                        res.send({
                            'status':500,
                            'msj': 'Contraseña incorrecta',
                            'data': []
                        });
                    }
                });
            })
            
          });
    })
}

exports.eliminarArchivo = async (req, res) => {
    let body = req.body;
    let id_file = body.id_file;
    let contrasena = body.password;
    //console.log(body)
    
    const sql = `SELECT * FROM archivo where id_file = ${id_file};`
    await db.connection.query(sql, function (error, results) {
        if (error) res.send({message: error});
        let id_user = results[0].id_user
        const sql2 = `select * from usuario where id_user = ${id_user};`
        db.connection.query(sql2, function (error, results2) {
            if (error) res.send({message: error});
            let compare = bcryptjs.compareSync(contrasena, results2[0].contrasena)
            if(compare){
                const sql3 = `DELETE FROM archivo WHERE id_file = ${id_file};`
                db.connection.query(sql3, function (error, results3) {
                if (error) res.send({message: error});
                console.log(results3);
                res.send({
                    'status':200,
                    'msj': 'Archivo eliminado con exito',
                    'data': []
                    });
                })
                
            }else{
                console.log("Contraseña incorrecta")
                res.send({
                    'status':500,
                    'msj': 'Contraseña incorrecta',
                    'data': []
                });
            }  
        })
        

    })
}

exports.eliminarArchivo2 = async (req, res) => {
    let body = req.body;
    let id_file = body.id_file;
    let contrasena = body.password;
    //console.log(body)
    
    const sql = `SELECT * FROM archivo where id_file = ${id_file};`
    const conn = db.connection.then(conn => {
        conn.query(sql, function (err, results) {
            if (err) throw err;
            let id_user = results[0].id_user
            const sql2 = `select * from usuario where id_user = ${id_user};`
            const conn2 = db.connection.then(conn2 => {
                conn2.query(sql2, function (err, results2) {
                    if (err) throw err;
                    let compare = bcryptjs.compareSync(contrasena, results2[0].contrasena)
                    if(compare){
                        const sql3 = `DELETE FROM archivo WHERE id_file = ${id_file};`
                        const conn3 = db.connection.then(conn3 => {
                            conn3.query(sql3, function (err, results3) {
                                if (err) throw err;
                                console.log(results3);
                                res.send({
                                    'status':200,
                                    'msj': 'Archivo eliminado con exito',
                                    'data': []
                                });
                            });
                        })
                        
                        
                    }else{
                        console.log("Contraseña incorrecta")
                        res.send({
                            'status':500,
                            'msj': 'Contraseña incorrecta',
                            'data': []
                        });
                    }
                });
            })
            
          });
    })
}

function uploadfile(nombre,extension,base64String){
    let folder = "";
    //Decodificamos la imagen
    let encodedImage = base64String;
    let decodedImage = Buffer.from(encodedImage,'base64');
    
    //Creamos el nombre nuevo de la foto
    const fileName = `${nombre}.${extension}`; 

    //Parametros para S3
    let bucketname = 'archivos-4-p1';
    let content_type_s3 = ""

    

    if(extension=="txt"){
        folder = "archivos/";
        content_type_s3 = `text/plain`
    }else if(extension=="png"){
        console.log("png")
        folder = "fotos/";
        content_type_s3 = `image/png`
    }else if(extension=="jpg"){
        folder = "fotos/";
        console.log("jpg")
        content_type_s3 = `image/jpeg`
    }else if(extension=="jpeg"){
        folder = "fotos/";
        console.log("jpg")
        content_type_s3 = `image/jpeg`
    }else if(extension=="pdf"){
        folder = "archivos/";
        content_type_s3 = `application/pdf`
    }

    
    
    let url = `${folder}${fileName}`;

    var uploadParamsS3 = {
        Bucket: bucketname,
        Key: url,
        Body: decodedImage,
        ContentType: content_type_s3,
        ACL: 'public-read',
      };
    
    bucket_s3.upload(uploadParamsS3, (err,data) => {
        if(err) {
            console.log('Error uploading file:', err);
            return " ";
        }
    });

    return url;
}