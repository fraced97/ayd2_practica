//const db_credentials = require('../models/conexion');
var db = require('../models/conexion')
//var mysql = require('mysql');
const bcryptjs = require('bcryptjs');
const { v4: uuidv4 } = require('uuid');
const AWS = require('aws-sdk');
const aws_cred = require('../models/aws-keys');
const bucket_s3 = new AWS.S3(aws_cred.s3);
//var conection = mysql.createPool(db_credentials);
//const db_credentials = require('../models/conexion');
//var conn = mysql.createPool(db_credentials);

exports.crear = async (req, res) => {
    let body = req.body;
    //let id_user = uuidv4();
    let password = body.password;
    let nombre = body.nombre;
    let correo = body.correo;
    //let nombreF = body.foto;
    //let extension = body.extension;
    let usuario = body.user;
    //let base64String = body.bases64;
    //console.log(body)
    const contraHash = await bcryptjs.hash(password, 8)
    
    const sql = `SELECT COUNT(username) AS dupli FROM usuario WHERE username="${usuario}";`
    await db.connection.query(sql, function (error, results) {
        if (error) res.send({message: error});
        if(results[0].dupli==0){
            console.log(contraHash)
            const sql2 = `SELECT COUNT(email) AS dupli2 FROM usuario WHERE email="${correo}";`
            db.connection.query(sql2, function (error, results2) {
                if (error) res.send({message: error});
                if(results2[0].dupli2==0){
                    
                        
                    const sql3 = `insert into usuario (username,email,nombre, password) values ('${usuario}','${correo}','${nombre}','${contraHash}');`
                    db.connection.query(sql3, function (error, results3) {
                    if (error) res.send({message: error});
                    console.log(results3);
                    res.send({
                        'status':200,
                        'msj': 'Usuario Registrado con exito',
                        'data': []
                        });
                    })
                    
                    
                }else{
                    console.log("Correo ya existe, elija otro correo por favor")
                    res.send({
                        'status':500,
                        'msj': 'Correo ya existe, elija otro correo por favor',
                        'data': []
                    });
                }  
            })
        }else{
            console.log("Usuario ya existe, elija otro nombre de usuario por favor")
            res.send({
                'status':500,
                'msj': 'Usuario ya existe, elija otro nombre de usuario por favor',
                'data': []
            });
        }

    })
}
exports.crear2 = async (req, res) => {
    let body = req.body;
    let id_user = uuidv4();
    let password = body.password;
    let nombre = body.nombre;
    let correo = body.correo;
    //let nombreF = body.foto;
    //let extension = body.extension;
    let usuario = body.user;
    //let base64String = body.bases64;
    //console.log(body)
    const contraHash = await bcryptjs.hash(password, 8)
    
    const sql = `SELECT COUNT(username) AS dupli FROM usuario WHERE username="${usuario}";`
    const conn = db.connection.then(conn => {
        conn.query(sql, function (err, result) {
            if (err) throw err;
            if(result[0].dupli==0){
                const sql2 = `SELECT COUNT(email) AS dupli2 FROM usuario WHERE email="${correo}";`
                const conn2 = db.connection.then(conn2 => {
                    conn2.query(sql2, function (err, result2) {
                        if (err) throw err;
                        if(result2[0].dupli2==0){
                            if(extension=="png"||extension=="jpg"||extension=="jpeg"){
                                let foto = uploadphoto(nombreF,extension,base64String);
                                let foto2 = `${nombreF}.${extension}`; 
                                const sql3 = `insert into usuario (username,email,nombre, contrasena,foto) values ('${usuario}','${correo}','${nombre}','${contraHash}','${foto2}');`
                                const conn3 = db.connection.then(conn3 => {
                                    conn3.query(sql3, function (err, result3) {
                                        if (err) throw err;
                                        console.log(result3);
                                        res.send({
                                            'status':200,
                                            'msj': 'Usuario Registrado con exito',
                                            'data': []
                                        });
                                      });
                                })
                            }else{
                                console.log("Extension de foto incorrecta")
                                res.send({
                                    'status':500,
                                    'msj': 'Extension de la fotografia no valida',
                                    'data': []
                                });
                            }
                        }else{
                            console.log("Correo ya existe, elija otro correo por favor")
                            res.send({
                                'status':500,
                                'msj': 'Correo ya existe, elija otro correo por favor',
                                'data': []
                            });
                        }
                    });
                })
            }else{
                console.log("Usuario ya existe, elija otro nombre de usuario por favor")
                res.send({
                    'status':500,
                    'msj': 'Usuario ya existe, elija otro nombre de usuario por favor',
                    'data': []
                });
            }
            
        });
    })
}


exports.login = async (req, res) => {
    let body = req.body;
    let password = body.password;
    let correo = body.user;
    let usuario = body.user;
    console.log(body)
    
    const sql = `select * from usuario where (username = "${usuario}") || (email = "${correo}");`
    await db.connection.query(sql, function (error, results) {
        if (error) res.send({message: error});
        if (results.length > 0) {
            let compare = bcryptjs.compareSync(password, results[0].password)
            if(compare){
                let archivo={
                    "id_user": results[0].id_user,
                    
                }
                let input ={
                    'status':200,
                    'data': {   
                        "id_user": results[0].id_user,
                        "username": results[0].username,
                        //"contrasena": results[0].contrasena,
                        //"email": results[0].email,
                        //"nombre": results[0].nombre,
                        //"foto": results[0].foto                     
                    }
                    //"archivos":[]
                    
                }
                console.log(input);
                res.send(input);
            }else{
                let user ={
                    'status':500,
                    'msj': 'Contraseña Incorrecta',
                    'data': []
                }
                console.log("Contraseña Incorrecta");
                res.send(user);
            }
        }else{
            let user ={
                'status':500,
                'msj': 'Usuario incorrecto',
                'data': []
            }
            console.log("Usuario incorrecto");
            res.send(user);
        }
    })
}
exports.login2 = async (req, res) => {
    let body = req.body;
    let password = body.password;
    let correo = body.user;
    let usuario = body.user;
    //console.log(body)
    const sql = `select * from usuario where (username = "${usuario}") || (email = "${correo}");`
    const conn = db.connection.then(conn => {
        conn.query(sql, function (err, result) {
            if (err) throw err;
            if (result.length > 0) {
                let compare = bcryptjs.compareSync(password, result[0].contrasena)
                if(compare){
                    let archivo={
                        "id_user": result[0].id_user,
                    }
                    let input ={
                        'status':200,
                        'data':{
                            "id_user": result[0].id_user,
                            "username": result[0].username,
                            //"contrasena": results[0].contrasena,
                            //"email": results[0].email,
                            //"nombre": results[0].nombre,
                            "foto": result[0].foto
                        }
                        //"archivos":[]
                        
                    }
                    console.log(input);
                    res.send(input);
                }else{
                    let user ={
                        'status':500,
                        'msj': 'Contraseña Incorrecta',
                        'data': []
                    }
                    console.log("Contraseña Incorrecta");
                    res.send(user);
                }
            }else{
                let user ={
                    'status':500,
                    'msj': 'Usuario incorrecto',
                    'data': []
                }
                console.log("Usuario incorrecto");
                res.send(user);
            }
            
        });
    })
}

exports.verUsuario = async (req, res) => {
    const sql = `SELECT * FROM usuario WHERE id_user <> ?`
    db.connection.query(sql,[req.params.id_user], (error, results) => {
        if (error) res.send({message: error});
        if(Object.keys(results).length!=0){
            let user ={
                'status':200,
                'msj': 'Usuarios registrados',
                'data': results
            }
            res.send(user);
        }else{
            let user ={
                'status':500,
                'msj': 'No hay usuarios registrados',
                'data': results
            }
            res.send(user);
        }
    })
}

exports.verUsuario2 = async (req, res) => {
    const sql = `SELECT id_user, username, foto, (SELECT COUNT(id_file) from  archivo WHERE archivo.id_user = usuario.id_user) as cantidad_archivos FROM usuario WHERE id_user <> (SELECT usuario.id_user, usuario.nombre FROM amigo, usuario WHERE usuario.id_user = amigo.id_friend AND amigo.id_user = ?)\
    (SELECT usuario.id_user, usuario.nombre FROM amigo, usuario WHERE usuario.id_user = amigo.id_user AND amigo.friend = ?)`
    const conn = db.connection.then(conn => {
        conn.query(sql,[req.params.id_user, req.params.id_user],function (err, results) {
            if (err) throw err;
            if(Object.keys(results).length!=0){
                let user ={
                    'status':200,
                    'msj': 'Usuarios registrados',
                    'data': results
                }
                res.send(user);
            }else{
                let user ={
                    'status':500,
                    'msj': 'No hay usuarios registrados',
                    'data': results
                }
                res.send(user);
            }
          });
    })
}

exports.verAmigos = (req,res) =>{
    const sql = `(SELECT usuario.id_user, usuario.nombre FROM amigo, usuario WHERE usuario.id_user = amigo.id_friend AND amigo.id_user = ?)\
    (SELECT usuario.id_user, usuario.nombre FROM amigo, usuario WHERE usuario.id_user = amigo.id_user AND amigo.friend = ?)`
    db.connection.then(
        conn =>{
            conn.query(sql, [req.params.id_user], (err, result) =>{
                if(err) {
                    res.send({
                        'status': 500,
                        'msj': 'Error al buscar los amigos'
                    })
                }
                else{
                    res.send({
                        'status':200,
                        'msj' : 'Amigos encontrados',
                        'data' : result
                    })
                }
            })
        }
    ).catch(
        res.send({
            'status': 500,
            'msj': 'Error al buscar los amigos'
        })
    )
}

exports.searchUsuario = (req,res) =>{
    const sql = `SELECT id_user, username, foto, (SELECT COUNT(id_file) from  archivo WHERE archivo.id_user = usuario.id_user) as cantidad_archivos FROM usuario WHERE id_user = ?`
    db.connection.then(
        conn =>{
            conn.query(sql, [req.params.id_user], (err, result) =>{
                if(err) {
                    res.send({
                        'status': 500,
                        'msj': 'Error al buscar los amigos'
                    })
                }
                else{
                    res.send({
                        'status':200,
                        'msj' : 'Amigos encontrados',
                        'data' : result
                    })
                }
            })
        }
    ).catch(
        res.send({
            'status': 500,
            'msj': 'Error al buscar los amigos'
        })
    )
}

exports.agregarAmigo = async (req, res) => {
    let body = req.body;
    let id_friend = body.id_friend;
    let id_user = body.id_user;
    //console.log(body)
    const sql = `SELECT * FROM amigo where id_user = ${id_user};`
    const conn = db.connection.then(conn => {
        conn.query(sql, function (err, result) {
            if (err) throw err;
            //if (result[0].id_friend != id_friend) {
                const sql2 = `INSERT INTO amigo (id_user, id_friend) VALUES (${id_user}, ${id_friend});`
                const conn2 = db.connection.then(conn2 => {
                    conn2.query(sql2, function (err, result2) {
                        let user ={
                            'status':200,
                            'msj': 'Usuario agregado como amigo',
                            'data': []
                        }
                        console.log("Usuario agregado como amigo");
                        res.send(user);
                    })
                })
            /*}else{
                let user ={
                    'status':500,
                    'msj': 'Usuario ya agregado como amigo',
                    'data': []
                }
                console.log("Usuario ya agregado como amigo");
                res.send(user);
            }*/
            
        });
    })
}

function uploadphoto(nombre,extension,base64String){
    let folder = "";
    //Decodificamos la imagen
    let encodedImage = base64String;
    let decodedImage = Buffer.from(encodedImage,'base64');
    
    //Creamos el nombre nuevo de la foto
    const fileName = `${nombre}.${extension}`; 

    //Parametros para S3
    let bucketname = 'archivos-4-p1';

    folder = "perfil/";
    let url = `${folder}${fileName}`;
    let content_type_s3 = ""
    if(extension=="png"){
        content_type_s3 = `image/png`
    }else if(extension=="jpg"){
        content_type_s3 = `image/jpeg`
    }else if(extension=="jpeg"){
        content_type_s3 = `image/jpeg`
    }

    var uploadParamsS3 = {
        Bucket: bucketname,
        Key: url,
        Body: decodedImage,
        ContentType: content_type_s3,
        ACL: 'public-read',
      };
      
    
    bucket_s3.upload(uploadParamsS3, (err,data) => {
        if(err) {
            console.log('Error uploading file:', err);
            return " ";
        }
    });

    return url;
}



