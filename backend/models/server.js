// LIBRERIAS PARA EL MANEJO DEL SERVER
const express = require('express');
const cors = require('cors');
//var routerUsuario = require('../routes/usuario.route')

class Server {
    
    constructor(){
        // CONFIGURACIÓN GENERAL DEL SERVIDOR
        this.app = express();
        this.port = 9000;
        this.usuarioPath = '/user';
        this.archivoPath = '/file';
        
        // MIDDLEWARES
        this.app.use(cors({
            origin: true,
            optionsSuccessStatus: 200
        }));

        this.app.use(express.json({
            limit: '10mb',
            extend: true
        }));

        this.app.use(express.urlencoded({
            limit:'10mb',
            extended: true
        }));

        // RUTAS DEL SERVER
        this.routes();
    }

    routes(){
       this.app.use(this.archivoPath, require('../routes/archivo.route'));
       this.app.use(this.usuarioPath, require('../routes/usuario.route'));
    }

    listen(){
        this.app.listen( this.port, () => {
            console.log( 'Servidor nodeJS corriendo en puerto: ', this.port );
        });
    }

}

module.exports = Server;