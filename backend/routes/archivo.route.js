const express = require('express')
const routerFile = express.Router()
var fileController = require('../controller/archivos.controller')
routerFile.post('/create', fileController.crear2);
routerFile.get('/', fileController.verArchivo2);
routerFile.get('/search/', fileController.verArchivo2);
routerFile.post('/upgrade', fileController.editarArchivo2);
routerFile.post('/delete', fileController.eliminarArchivo2);

module.exports = routerFile