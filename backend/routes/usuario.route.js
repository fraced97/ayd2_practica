const express = require('express')
const routerUsuario = express.Router()
var usuarioController = require('../controller/usuarios.controller')
routerUsuario.post('/signup', usuarioController.crear);
routerUsuario.post('/login', usuarioController.login);
routerUsuario.get('/:id_user', usuarioController.verUsuario);
routerUsuario.get('/search/:id_user', usuarioController.searchUsuario);
routerUsuario.get('/friends/:id_user', usuarioController.verAmigos);
routerUsuario.post('/agregar', usuarioController.agregarAmigo);
module.exports = routerUsuario