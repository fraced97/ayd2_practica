# imports
import json
import sys
import time
import os
import random
import string
import time
#import datetime
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.common.by import By
#from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from random import randrange
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
#201700350
SELENIUM_HUB = 'http://34.125.41.136:4445/wd/hub'
driver = webdriver


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def test1():

    try:
        print("starting prueba registro")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX,
        )

        print("opening the page")
        driver.get('http://35.232.140.40/signup')
        time.sleep(4)
        

        search_u = driver.find_element_by_id("Nombre")
        search_u.clear()
        search_u.send_keys("Fredy Ramírez Moscoso")
        time.sleep(1)
        print("escribiendo nombre")

        search_e = driver.find_element_by_id("email")
        search_e.clear()
        search_e.send_keys("ejemplo"+id_generator(2)+"@gmail.com")
        time.sleep(1)
        print("escribiendo correo")

        search_p = driver.find_element_by_id("user")
        search_p.clear()
        search_p.send_keys("Fraced"+id_generator(4))
        time.sleep(1)
        print("se escribio usuario")

        search_l = driver.find_element_by_id("password")
        search_l.clear()
        search_l.send_keys("1234")
        time.sleep(1)
        print("se inserto el contrasenia")

        search_n = driver.find_element_by_id("valida")
        search_n.clear()
        search_n.send_keys("1234")
        time.sleep(1)
        print("se escribio password")

        search_btn = driver.find_element_by_id("Reg")
        search_btn.click()
        time.sleep(4)
        print("se inserto el usuario")


        print("prueba de registro exitosa")
        time.sleep(10)
        driver.quit()
        return(1)
    except:
        driver.quit()
        print("error en prueba de registro")
        return (0)


def test2():

    try:
        print("starting prueba registro")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX,
        )

        print("opening the page")
        driver.get('http://35.232.140.40/login')
        time.sleep(4)
        print("se abrio la pagina")
        

        search_p = driver.find_element_by_id("email")
        search_p.clear()
        search_p.send_keys("Fraced")
        time.sleep(1)
        print("se escribio usuario")

        search_e = driver.find_element_by_id("password")
        search_e.clear()
        search_e.send_keys("1234")
        time.sleep(1)
        print("se escribio password")

        search_btn = driver.find_element_by_id("iniciar2")
        search_btn.click()
        time.sleep(4)
        print("se inserto el usuario")

        print("prueba de registro exitosa")
        time.sleep(10)
        driver.quit()
        return(1)
    except:
        driver.quit()
        print("error en prueba de registro")
        return (0)


def __main__():
    msgError = "Error en las pruebas funcionales."
    if(test1() == 0):
        raise Exception(msgError)

    if(test2() == 0):
        raise Exception(msgError)
    print("Pruebas Funcionales Exitosas")


__main__()
